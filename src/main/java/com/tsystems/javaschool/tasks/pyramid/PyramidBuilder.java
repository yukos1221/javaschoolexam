package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        int numbOfElements=0;
        int numbOfRows=0;
        boolean isOkey=false;
        ListIterator<Integer> firstIter = inputNumbers.listIterator();
        while (firstIter.hasNext()) {
            if ((firstIter.next()==null)||(numbOfElements>5040)) throw new CannotBuildPyramidException();
            numbOfElements++;
        }
        inputNumbers.sort(Comparator.comparing(Integer::byteValue));
        ListIterator<Integer> secIter = inputNumbers.listIterator();

        int expectedNumb=0;
        for (int i=0; i<8; i++) {
            expectedNumb+=i;
            if (expectedNumb==numbOfElements) { isOkey=true; numbOfRows=i; break; }
        }
        if (!isOkey) throw new CannotBuildPyramidException();
        int numbOfColumns=numbOfRows*2 -1;

        int[][] mas = new int[numbOfRows][numbOfColumns];
        for (int i=0; i<numbOfRows; i++) {
            for (int j=0; j<numbOfColumns; j++) {
                mas[i][j]=0;
            }
        }
        for (int i=0;i<numbOfRows; i++) {
            int n1 = numbOfColumns/2-i;
            for (int j=0; j<(i+1); j++) {
                mas[i][n1] = secIter.next();
                n1+=2;
            }
        }
        return mas;
    }
}
