package com.tsystems.javaschool.tasks.calculator;

import java.util.HashSet;
import java.util.Set;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (!isOkey(statement)) return null;                            //check the statement for correctness

        String stat2 = doMath(statement);
        while (!stat2.equals("nothing")) {                              //execute the code until there are arithmetic operations
            statement = stat2;
            stat2=doMath(statement);
            if (stat2==null) return null;
        }
        return statement;
    }

    private boolean isOkey(String statement) {
        if ((statement==null)||(statement.equals(""))) return false;

        int openParantheses=0, closeParantheses=0;
        for (int i=0; i<statement.length(); i++) {
            if (statement.charAt(i)>57||statement.charAt(i)<40||statement.charAt(i)==44) return false;   //invalid symbols
            if (statement.charAt(i)==41) {
                closeParantheses++;
                if (openParantheses<closeParantheses) return false;
            }
            else if (statement.charAt(i)==40) openParantheses++;
        }
        if (openParantheses!=closeParantheses) return false;
        if (statement.contains("++")||statement.contains("--")||statement.contains("**")||statement.contains("//")||statement.contains("..")) return false;
        return true;
    }

    private String doMath(String input) {
        if (input.contains("(")) return paranth(input);
        if (input.contains("*")||input.contains("/")) {
            if (input.contains(".")) return multdivDouble(input);
            else return multdiv(input);
        }
        if (input.contains("+")||input.contains("-")) return addsub(input);
        return "nothing";
    }

    private String paranth(String input) {
        int n1 = input.indexOf('(');
        int n2 = input.indexOf(')');
        String par = input.substring(n1+1,n2);
        return input.substring(0,n1)+ evaluate(par) +input.substring(n2+1);     //make a recursive function call
    }

    private String multdiv(String input) {
        int n1 = input.indexOf('*');
        int n2 = input.indexOf('/');
        if (n1==-1) return division(input);
        if (n2==-1) return multpl(input);
        if (n2<n1) return division(input);
        else return multpl(input);
    }

    private String multdivDouble(String input) {
        int n1 = input.indexOf('*');
        int n2 = input.indexOf('/');
        if (n1==-1) return divisionDouble(input);
        if (n2==-1) return multplDouble(input);
        if (n2<n1) return divisionDouble(input);
        else return multplDouble(input);
    }

    private String addsub(String input) {
        int n1 = input.indexOf('+');
        int n2 = input.indexOf('-');
        if ((input.charAt(0)=='-')&&(input.contains("+"))) return addition(input);
        if (n1==-1) return subtrack(input);
        if (n2==-1) return addition(input);
        if (n2<n1) return subtrack(input);
        else return addition(input);
    }

    private String multpl(String input) {
        int n1 = input.indexOf('*');
        int n0=findN0(input, '*');
        int n2=findN2(input, '*');
        int first = Integer.parseInt(input.substring(n0,n1));
        int second = Integer.parseInt(input.substring(n1+1,n2+1));
        return input.substring(0,n0)+first*second+input.substring(n2+1);
    }

    private String division(String input) {
        int n1 = input.indexOf('/');
        int n0=findN0(input, '/');
        int n2=findN2(input, '/');
        int first = Integer.parseInt(input.substring(n0,n1));
        int second = Integer.parseInt(input.substring(n1+1,n2+1));
        if (second==0) return null;
        return input.substring(0,n0)+ first/second +input.substring(n2+1);
    }

    private String addition(String input) {
        int n1 = input.indexOf('+');
        int n0=findN0(input, '+');
        int n2=findN2(input, '+');
        int first = Integer.parseInt(input.substring(n0,n1));
        int second = Integer.parseInt(input.substring(n1+1,n2+1));
        return input.substring(0,n0)+ (first+second) +input.substring(n2+1);
    }

    private String subtrack(String input) {
        int n1 = input.indexOf('-');
        if (n1==0) return "nothing";
        int n0=findN0(input, '-');
        int n2=findN2(input, '-');
        int first = Integer.parseInt(input.substring(n0,n1));
        int second = Integer.parseInt(input.substring(n1+1,n2+1));
        return input.substring(0,n0)+ (first-second) +input.substring(n2+1);
    }

    private String multplDouble(String input) {
        int n1 = input.indexOf('*');
        int n0=findN0(input, '*');
        int n2=findN2(input, '*');
        double first = Double.parseDouble(input.substring(n0,n1));
        double second = Double.parseDouble(input.substring(n1+1,n2+1));
        return input.substring(0,n0)+first*second+input.substring(n2+1);
    }

    private String divisionDouble(String input) {
        int n1 = input.indexOf('/');
        int n0=findN0(input, '/');
        int n2=findN2(input, '/');
        double first = Double.parseDouble(input.substring(n0,n1));
        double second = Double.parseDouble(input.substring(n1+1,n2+1));
        if (second==0) return null;
        return input.substring(0,n0)+ first/second +input.substring(n2+1);
    }

    private int findN0(String input, char c) {      //method for finding the first argument of an arithmetic operation
        int n1 = input.indexOf(c);
        char[] localCharArray = input.toCharArray();
        int index = n1-1;
        while (index!=0) {
            if ((input.charAt(index)!='+')&&(input.charAt(index)!='-')&&(input.charAt(index)!='*')&&(input.charAt(index)!='/'))  index--;
            else { break; }
        }
        if (index==0) return index; else return index+1;
    }

    private int findN2(String input, char c) {      //method for finding the second argument of an arithmetic operation
        int n1 = input.indexOf(c);
        int index=n1+1;
        while (index!=input.length()) {
            if ((input.charAt(index)!='+')&&(input.charAt(index)!='-')&&(input.charAt(index)!='*')&&(input.charAt(index)!='/'))  index++;
            else { break; }
        }
        if (input.charAt(n1+1)=='-') return index+1; else return index-1;
    }
}
